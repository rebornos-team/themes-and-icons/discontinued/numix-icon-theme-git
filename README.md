# numix-icon-theme-git

Base icon theme from the Numix project

https://github.com/numixproject/numix-icon-theme

How to clone this repo:

```
git clone https://gitlab.com/reborn-os-team/themes-and-icons/numix-icon-theme-git.git
```

